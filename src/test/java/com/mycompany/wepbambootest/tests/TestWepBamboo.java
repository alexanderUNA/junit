/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wepbambootest.tests;

import java.time.Duration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

/**
 *
 * @author alex
 */
public class TestWepBamboo {

    private WebDriver driver;

    @BeforeEach
    public void setTestLogin() {
        System.setProperty(
                "webdriver.edge.driver", "src/main/java/drivers/msedgedriver");
        EdgeOptions options = new EdgeOptions();

        this.driver = new EdgeDriver(options);

        this.driver.get("http://localhost:8080/WepBamboo/");
        //options.addArguments("--headless");
    }

    @AfterEach
    public void turnDown() {
        this.driver.quit();
    }

    @Test
    @DisplayName("Prueba con valor incorrecto")
    public void testTextBoxFailValue() {

        WebElement searchBox = driver.findElement(By.id("name"));

        searchBox.sendKeys("eelenium");
        this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));

        Assertions.assertNotEquals("Selenium", searchBox.getAttribute("value"));
    }

    @Test
    @DisplayName("Prueba con valor correcto")
    public void testTextBoxCorrectValue() {

        WebElement searchBox = driver.findElement(By.id("name"));

        searchBox.sendKeys("Selenium");
        this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));

        Assertions.assertEquals("Selenium", searchBox.getAttribute("value"));
    }

    @Test
    @DisplayName("Valor del title de la página")
    public void testTitlePage() {

        Assertions.assertEquals("Start Page", this.driver.getTitle());
    }

    @Test
    @DisplayName("Prueba numeral con valor incorrecto")
    public void testTextBoxNumberFailValue() {

        WebElement searchBox = driver.findElement(By.id("num"));

        searchBox.sendKeys("w");
        this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));

        Assertions.assertNotEquals("4", searchBox.getAttribute("value"));
    }

    @Test
    @DisplayName("Prueba rumeral con valor correcto")
    public void testTextBoxNumberCorrectValue() {

        WebElement searchBox = driver.findElement(By.id("num"));

        searchBox.sendKeys("4");
        this.driver.manage().timeouts().implicitlyWait(Duration.ofMillis(1500000));

        Assertions.assertEquals("4", searchBox.getAttribute("value"));
    }
}
